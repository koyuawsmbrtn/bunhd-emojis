#!/bin/sh

mkdir -p export
rm -f export/*

mkdir -p export_flip
rm -f export_flip/*

find . -type f \( -iname "*.svg" ! -iname ".*" ! -iname "a*" \) -print0 | parallel -0 'x={.}; inkscape -z -e "export/${x#./}.png" "{}"' {} \;
./animate.sh abunhd 2 20:100
./animate.sh abunhdcry 2 20:100
./animate.sh abunhdhappy 2 20:100
./animate.sh abunhdhop 4 9:100
./animate.sh abunhdhappyhop 4 9:100
./animate.sh abunhdowohop 4 9:100
./sadpat.sh
cp LICENSE export/

cd export
cp ./bunhd* ../export_flip/
cp ./LICENSE ../export_flip/


FILELIST=`find . -type f -iname '*.png' -exec sh -c 'x=${0#./}; printf "%s:%s|" ${x%.png} $x' {} \;`
jq -Rn 'input | split("|") | map(split(":") | { key: .[0], value: .[1] }) | from_entries' <<< "${FILELIST%|}" > bunhd.json

zip bunhd.zip *.png
zip bunhd.zip LICENSE
CHECKSUM=`sha256sum -z bunhd.zip | awk '{ print $1 }'`

cd ../export_flip

rm bunsleep.png
find . -type f -iname '*.png' -exec sh -c 'x=${0#./}; mv $x rev$x' {} \;
mogrify -flop *.png
FILELIST=`find . -type f -iname '*.png' -exec sh -c 'x=${0#./}; printf "%s:%s|" ${x%.png} $x' {} \;`
jq -Rn 'input | split("|") | map(split(":") | { key: .[0], value: .[1] }) | from_entries' <<< "${FILELIST%|}" > bunhd_flip.json

zip bunhd_flip.zip *.png
zip bunhd_flip.zip LICENSE
CHECKSUM_FLIP=`sha256sum -z bunhd_flip.zip | awk '{ print $1 }'`

cd ../export

mv ../export_flip/bunhd_flip.zip ./
mv ../export_flip/bunhd_flip.json ./

rm -f *.png
rm -f ../export_flip/*

printf '{
    "bunhd": {
        "description": "High-res version of the bun emojis",
        "files":       "bunhd.json",
        "homepage":    "https://www.feuerfuchs.dev/projects/bunhd-emojis/",
        "src":         "https://www.feuerfuchs.dev/projects/bunhd-emojis/bunhd.zip",
        "src_sha256":  "%s",
        "license":     "Apache 2.0"
    },
    "bunhd_flip": {
        "description": "High-res version of the bun emojis (flipped version)",
        "files":       "bunhd_flip.json",
        "homepage":    "https://www.feuerfuchs.dev/projects/bunhd-emojis/",
        "src":         "https://www.feuerfuchs.dev/projects/bunhd-emojis/bunhd_flip.zip",
        "src_sha256":  "%s",
        "license":     "Apache 2.0"
    }
}' $CHECKSUM $CHECKSUM_FLIP > manifest.json
