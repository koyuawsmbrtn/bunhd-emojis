#!/bin/sh

SOURCEBASE=abunhdsadpat
MAX=17
DELAY=20:100
SOURCESVG=`printf "%s.svg" $SOURCEBASE`
TARGETSVG=`printf "export/%s.png" $SOURCEBASE`


mkdir -p export_tmp
rm -f export_tmp/*


counter=1
while [ $counter -le $MAX ]
do
    source=`printf "%s%s.svg" $SOURCEBASE $counter`
    file=`printf "export_tmp/%s.png" $counter`
    inkscape -z -e $file $source
    ((counter++))
done


declare -a args
args+=(export_tmp/1.png $DELAY)
args+=(export_tmp/2.png $DELAY)

args+=(export_tmp/3.png 9:100)
args+=(export_tmp/4.png 9:100)
args+=(export_tmp/5.png 9:100)
args+=(export_tmp/6.png 9:100)

args+=(export_tmp/7.png $DELAY)
args+=(export_tmp/8.png $DELAY)
args+=(export_tmp/9.png $DELAY)
args+=(export_tmp/10.png $DELAY)
args+=(export_tmp/11.png $DELAY)
args+=(export_tmp/12.png $DELAY)

args+=(export_tmp/13.png 9:100)
args+=(export_tmp/14.png 9:100)
args+=(export_tmp/15.png 9:100)
args+=(export_tmp/16.png 9:100)

args+=(export_tmp/17.png $DELAY)

apngasm -o $TARGETSVG ${args[@]}
rm -f export_tmp/*
